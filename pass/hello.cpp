#include "passes.h"
#include "llvm/Support/WithColor.h"
#include "llvm/IR/IRBuilder.h"
// For llvm::instructions(Function &)
#include "llvm/IR/InstIterator.h"


using namespace llvm;

PreservedAnalyses Hello::run(Function &F, FunctionAnalysisManager &) {
	auto CyanErr = []() { return WithColor(errs(), raw_ostream::Colors::CYAN); };
	CyanErr() << "Hello from " << F.getName() << "\n";

  return PreservedAnalyses::all();
}
