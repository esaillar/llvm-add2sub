#include "passes.h"

#include "llvm/IR/IRBuilder.h"
// For llvm::instructions(Function &)
#include "llvm/IR/InstIterator.h"


using namespace llvm;

PreservedAnalyses AddToSub::run(Function &F, FunctionAnalysisManager &) {
	IRBuilder<> Builder{&F.getEntryBlock()};

	// Iterate over Add instructions of F
		// Found add
			// Set insertion point
			// create sub operand
			// Move the insertion point and replace the Add by a Sub.
	// Cleanup unused instructions

	return PreservedAnalyses::all();
}
